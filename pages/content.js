import React, { Component } from 'react'
import swal from 'sweetalert';

export default class Content extends Component {

    constructor(props) {
        super(props);

        this.state = {
            enNumber: '',
            enText: '',
            deNumber: '',
            deText: '',
        };

        this.handleEnNumberChange = this.handleEnNumberChange.bind(this);
        this.handleEnTextChange = this.handleEnTextChange.bind(this);
        this.handleDeNumberChange = this.handleDeNumberChange.bind(this);
        this.handleDeTextChange = this.handleDeTextChange.bind(this);
    }

    handleEnNumberChange(e) {
        let number
        if (e.target.value !== '') {
            if (e.target.value < 26) {
                number =  parseInt(e.target.value)
            }
            else {
                swal({
                    title: "Perhatiannya Ya Mas / Mba",
                    text: "Angka Yang di Perbolehkan Yaitu 1-26. Terima Kasih",
                    icon: "info",
                    button: "Asyiaap",
                });
                number = ''
            }
        }
        else {
            number = ''
        }
        this.setState({
            enNumber: number
        });
    }

    handleEnTextChange(e) {
        this.setState({
            enText: e.target.value
        });
    }

    handleDeNumberChange(e) {
        let number
        if (e.target.value !== '') {
            if (e.target.value < 26) {
                number =  parseInt(e.target.value)
            }
            else {
                swal({
                    title: "Perhatiannya Ya Mas / Mba",
                    text: "Angka Yang di Perbolehkan Yaitu 1-26. Terima Kasih",
                    icon: "info",
                    button: "Asyiaap",
                });
                number = ''
            }
        }
        else {
            number = ''
        }
        this.setState({
            deNumber: number
        });
    }

    handleDeTextChange(e) {
        this.setState({
            deText: e.target.value
        });
    }

    render() {

        const { Cipher } = require("js-cipher");
        const cipher = new Cipher();

        return (
            <div className="main">
                <main style={{ overflow: 'auto', margin: 'auto' }}>
                    <div className="title" style={{ zoom: '100%',  padding: '0px 30px', marginTop: '1rem', }}>
                        <h1 className="title" style={{ lineHeight: '105px' }}>
                            <a href="#" style={{ textDecoration: 'underline', marginLeft: '20px', fontWeight: '300' }}>
                                Caesar Cipher App!
                            </a>
                        </h1>

                        <p className="description" style={{ color: '#333', lineHeight: '45px' }}>
                            <b>Present by</b><br/>
                            <code style={{ background: '#eee', marginRight: '10px', color: '#333', fontSize: '19px' }}>
                                <b>Kelompok 3</b>
                            </code>
                            Keamanan Informasi dan Jaringan 5A
                        </p>
                        <div>
                            <p style={{ marginTop: '25px', lineHeight: '30px', borderTop: '1px solid #333', paddingTop: '10px', fontSize: '21px' }}>
                                <small>
                                    <small>
                                        (
                                            Muhammad Zaidan / 1803015050, 
                                            Dimas Adi Prananda / 1803015027,
                                            Rivaldo Gabrielleo / 1803015145, <br/>
                                            Diyka Renaldy / 1703015052, 
                                            Muhammad Fathan A / 1703015208
                                        )
                                    </small>
                                </small>
                            </p>
                        </div>
                    </div>

                    <div style={{
                        borderRadius: '5px',
                        marginTop: '1.3rem',
                        overflowY: 'auto',
                        width: '100%',
                        padding:  '0px 30px',
                        maxHeight: '470px',
                        background: '#fff'
                    }}>
                        <div className="grid" style={{
                            alignItems: 'normal',
                            marginTop: '35px',
                            borderRadius: '5px',
                            width: '100%'
                        }}>
                            <h2 style={{ width: '93%', textAlign: 'left', color: '#888' }}>
                                Encrypt
                            </h2>
                            <div className="card" style={{ background: '#fff' }}>
                                <div style={{ marginBottom: '35px' }}>
                                    <h3> Shift </h3>
                                    <input type="number" min="1" max="26" style={{ width: '100%', padding: '10px', fontSize: '15px', fontFamily: 'arial' }}
                                    placeholder="Angka yang dimasukkan adalah antara 1-26" onChange={this.handleEnNumberChange}
                                    value={this.state.enNumber} />
                                </div>
                                <div style={{ marginTop: '35px' }}>
                                    <h3> Plain Text </h3>
                                    <textarea style={{ width: '100%', padding: '10px', fontSize: '15px', fontFamily: 'arial' }} cols="30" rows="5"
                                    placeholder="Tulis disini kata/kalimat yang akan di enkripsi" onChange={this.handleEnTextChange}
                                    value={this.state.enText} ></textarea>
                                </div>
                            </div> 
                            <div className="card" style={{ background: '#fff' }}>
                                <div style={{ marginBottom: '35px' }}>
                                    <h3> Cipher Text </h3>
                                    <textarea style={{ 
                                        width: '100%', 
                                        padding: '10px 0px', 
                                        fontSize: '15px', 
                                        fontFamily: 'arial', 
                                        background: '#fff', 
                                        borderRadius: '5px',
                                        border: 'none', 
                                        resize: 'none',
                                    }} 
                                    cols="30" rows="10" disabled placeholder="Hasil ecrypt menggunakan caesar cipher" 
                                    value={this.state.enNumber != '' && this.state.enText != '' ? 
                                    cipher.encrypt(this.state.enText, this.state.enNumber)  : '' } ></textarea>
                                    <small>
                                        {this.state.enNumber != '' && this.state.enText != '' ? "*kalo gapercaya coba aja decrypt :'v" : ''}
                                    </small>
                                </div>
                            </div>
                        </div>
                        <div className="grid" style={{
                            alignItems: 'normal',
                            marginTop: '35px',
                            borderRadius: '5px',
                            width: '100%',
                        }}>
                            <h2 style={{ width: '93%', textAlign: 'left', color: '#888' }}>
                                Decrypt
                            </h2>
                            <div className="card" style={{ background: '#fff' }}>
                                <div style={{ marginBottom: '35px' }}>
                                    <h3> Shift </h3>
                                    <input type="number" min="1" max="26" style={{ width: '100%', padding: '10px', fontSize: '15px', fontFamily: 'arial' }}
                                    placeholder="Angka yang dimasukkan adalah antara 1-26" onChange={this.handleDeNumberChange}
                                    value={this.state.deNumber} />
                                </div>
                                <div style={{ marginTop: '35px' }}>
                                    <h3> Plain Text </h3>
                                    <textarea style={{ width: '100%', padding: '10px', fontSize: '15px', fontFamily: 'arial' }} cols="30" rows="5"
                                    placeholder="Tulis disini kata/kalimat yang akan di dekripsi" onChange={this.handleDeTextChange}
                                    value={this.state.deText} ></textarea>
                                </div>
                            </div>
                            <div className="card" style={{ background: '#fff' }}>
                                <div style={{ marginBottom: '35px' }}>
                                    <h3> Cipher Text </h3>
                                    <textarea style={{ 
                                        width: '100%', 
                                        padding: '10px 0px', 
                                        fontSize: '15px', 
                                        fontFamily: 'arial', 
                                        background: '#fff', 
                                        borderRadius: '5px',
                                        border: 'none', 
                                        resize: 'none',
                                    }} 
                                    cols="30" rows="10" disabled placeholder="Hasil ecrypt menggunakan caesar cipher" 
                                    value={this.state.deNumber != '' && this.state.deText != '' ? 
                                    cipher.decrypt(this.state.deText, this.state.deNumber)  : '' } ></textarea>
                                    <small>
                                        {this.state.deNumber != '' && this.state.deText != '' ? "*kalo gapercaya coba aja encrypt :'v" : ''}
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>

                <style jsx>{`
                    ::-webkit-scrollbar {
                        -webkit-appearance: none;
                        width: 9px;
                    }

                    ::-webkit-scrollbar-thumb {
                        border-radius: 4px;
                        background-color: rgba(0, 0, 0, .5);
                        box-shadow: 0 0 1px rgba(255, 255, 255, .5);
                    }
                    .container {
                        min-height: 100vh;
                        padding: 0 0.5rem;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }

                    main {
                        padding: 5rem 0;
                        flex: 1;
                        display: flex;
                        flex-direction: column;
                        justify-content: center;
                        align-items: center;
                    }

                    footer {
                        background: #fff;
                        z-index: 999 !important;
                        width: 100%;
                        height: 50px;
                        border-top: 1px solid #eaeaea;
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }

                    footer img {
                        margin-left: 0.5rem;
                    }

                    footer a {
                        display: flex;
                        justify-content: center;
                        align-items: center;
                    }

                    a {
                        color: inherit;
                        text-decoration: none;
                    }

                    .title a {
                        color: #0070f3;
                        text-decoration: none;
                    }

                    .title a:hover,
                    .title a:focus,
                    .title a:active {
                        text-decoration: underline;
                    }

                    .title {
                        margin: 0;
                        line-height: 1.15;
                        font-size: 4rem;
                    }

                    .title,
                    .description {
                        text-align: center;
                    }

                    .description {
                        line-height: 1.5;
                        font-size: 1.5rem;
                    }

                    code {
                        background: #fafafa;
                        border-radius: 5px;
                        padding: 0.75rem;
                        font-size: 1.1rem;
                        font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
                            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
                    }

                    .grid {
                        display: flex;
                        align-items: center;
                        justify-content: center;
                        flex-wrap: wrap;

                        max-width: none;
                    }

                    .card {
                        margin: 1rem;
                        flex-basis: 45%;
                        padding: 1.5rem;
                        text-align: left;
                        color: inherit;
                        text-decoration: none;
                        border: 1px solid #eaeaea;
                        border-radius: 5px;
                        transition: color 0.15s ease, border-color 0.15s ease;
                    }

                    .card:hover,
                    .card:focus,
                    .card:active {
                        color: #333;
                        /* border-color: #333; */
                    }

                    .card h3 {
                        margin: 0 0 1rem 0;
                        font-size: 1.5rem;
                    }

                    .card p {
                        margin: 0;
                        font-size: 1.25rem;
                        line-height: 1.5;
                    }

                    .logo {
                        height: 1em;
                    }

                    @media (max-width: 600px) {
                        .grid {
                            width: 100%;
                            flex-direction: column;
                        }
                    }
                `}</style>

                <style jsx global>{`
                    html,
                    body {
                        padding: 0;
                        margin: 0;
                        font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
                            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
                            sans-serif;
                    }

                    * {
                        box-sizing: border-box;
                    }
                `}</style>
            </div>
        )
    }
}