import Head from 'next/head'
import Content from './content'

export default function Home() {

    return (
        <div className="container" style={{ 
            zoom: '90%', 
        }}>
            <Head>
                <title> Caesar Cipher App! </title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Content/>
        </div>
    )
}